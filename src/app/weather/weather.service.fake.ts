import { Observable, of } from 'rxjs';
import { IWeatherService } from './weather.service';
import { ICurrentWeather } from '../interface';

export class WeatherServiceFake implements IWeatherService {
  private fakeWeather: ICurrentWeather = {
    city: 'Richmond',
    country: 'US',
    image: '',
    date: 1485789600,
    temperature: 280.32,
    description: 'Cloudy with chances of meatball'
  };

  getCurrentWeather(city: string, country: string): Observable<ICurrentWeather> {
    return of(this.fakeWeather);
  }

}
